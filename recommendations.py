import random
      
def recommendations(final_dict,reco_list,final_list,user,num_books,found):
    no_must=0
    current=[]
    for i in range(len(reco_list)): #if a book in the reco list is already in the final list skip it
        if reco_list[i] in final_list:
            continue
        else:
            final_list.append(reco_list[i]) #else append it in the list
            current.append(reco_list[i])
    if num_books<len(final_list): #if the length of the final list is greater thant the number of recommendations
        no_must=len(final_list)-num_books#find by how many books this number is exceeded
        current=current[:-(no_must)] 
        final_list=final_list[:-(no_must)] #subtract the exceed value of books from the final list
        final_dict[user]=current
        found=True
    else:
        final_dict[user]=current
    return final_dict,final_list,found #return a dictionary with {user:[books,authos]}, and the final_list





def new_user_details(user_name,books_list): #Function to add non-existing user and his retings in ratings.txt
    accepted_values=["-5","-3","-1","0","1","3","5"] #list of all accepted rates
    random_book_list=[]
    new_list=[]
    rating=""
    with open("ratings.txt", "a") as b:
        b.write(user_name+"\n") #append in the file the new_user_name
        for i in range(int(len(books_list)*1/5)): #ask the user to add the ratings of 20% of all the books, books rated randomly
            random_book_list.append(random.randint(0,len(books_list)))    
        print("""                             5 Hated it!
                            -3 Didn’t like it
                             0 Haven’t read it
                             1 OK
                             3 Liked it!
                            5 Really liked it!""","\n")
        for i in range(len(books_list)):
            found=False
            if i in random_book_list: 
                print("Add Your rating of the book,",books_list[i][1] , "of the author,", books_list[i][0]) #Show the name of the book and authot asked to be rated
                while found==False: #if input is not within the accepted_values list ask to input again 
                    value=(input())
                    if value not in accepted_values: #check if value entered is in the list
                        print("This is not an acceptable rating. Try again!")
                    else:
                        found=True
                        
                rating+= str(value)+ " "   
            else:  
                rating+= "0 "  #if input is not asked for a book add 0, meaning that it is not read by the new user      
        b.write(rating+"\n")
        b.close()  #close file      
                
                
def books_rec(user,books_list,ratings_dict): #Function to find all the books recommended by the most suitable user
    new_list=[]
    for key,value in ratings_dict[user].items(): #if rating is 3 or 5 then the book is suitable for recommendation
        if int(value) == 5 or int(value) ==3:
            new_list.append(key)
    return new_list

def unread_books(user_name,ratings_dict): #function to identify all the unread books of the user selected
    new_list=[]
    for key,value in ratings_dict[user_name].items():
        if int(value) == 0: #if rating is 0, then the book is unread
            new_list.append(key)
    return new_list


def dot_product(current_val,value): #calculate the dot_product of the ratings of each book and user
    dot_pro=0
    for i in range(1,len(current_val)):
        dot_pro+= int(current_val[i]) * int(value[i]) #find the dot product of all ratings of the 2 users used
    return dot_pro


def similarity(user_name,books_list,ratings_dict):
    current_val=ratings_dict[user_name] #current_val = all the ratings of the user entered
    new_list=[]
    sorted_name_list=[]
    highest_rating={}
    for key,value in ratings_dict.items():
        if key==user_name: #if the key of the dictionary is the same as the user entered, skip it
            continue
        else:
            highest_rating[key]=dot_product(current_val,value) #call the dot product function to find the dot product of corresonding books
            new_list.append(dot_product(current_val,value)) #add all the dot products according to each user in the new_list
            
    a = sorted(highest_rating.items(), key=lambda x: x[1],reverse=True)  #sort the list from highest to lowest (hghest is most compatible)          
    sorted_list=sorted(new_list) ###THIS HERE SEEE IT AND JUDGE
    for i in a:
        sorted_name_list.append(i[0]) 
    return sorted_name_list



def number_of_recommenations(): #Insert how many books you would like to be recommended, default number is 10.
    found=False
    yes_list=["Yes","yes","YES"]
    no_list=["No","no","NO"]
    no_reco=0
    try: #If the input is not valid ask to re-enter the value
        data = str(input("Would you like to use the default number of recommendations of 10 books?, Insert Yes or No! "))
        assert data in yes_list or data in no_list
    except:
        print("This is not a valid input.")
        no_reco=number_of_recommendations() #Try again by calling again
        
    finally:
        if data in no_list: #if input is no insert your own value of recommendations
            while found==False:
                try:
                    no_reco=int(input("Please input how many recommendations you would like."))
                    found=True
                except:
                    print("This is not a valid value, pls try again")
                    continue
                    
        elif data in yes_list: #if input is yes, use the default value of recommendations 10
            no_reco=10
        else: #else try again by calling again
            no_reco=number_of_recommenations()
    return no_reco        
            
 

def dict_make(ratings):#Make a nested dictionary to indicate the the book and the ratinf, format= {1:5,2:3,3:5...}
    rating_dict={}
    rating=ratings.split(" ")
    j=1
    for i in range(len(rating)-1):
        rating_dict[j]=rating[i]
        j+=1
    return rating_dict    
    


def book():  #Make a list of all the authors,book_names lists from the books file
    books_list=[]
    try:
        books=open("books.txt") #open the books.txt
    except:
        print("Threre was an error in loading the file.")
    for line in books.readlines():
        new_line=line.replace("\n","").split(",") 
        books_list.append(new_line) #add the [author,book_name] to the book_list list 
    return books_list #format of the book_list is [[author,book_name],[author,book_name]...]

def rating():#Make a dictionary all the customers and their book ratings
    i=0
    new_list=[]
    ratings_dict={}
    try:
        rating=open("ratings.txt") #open the ratings.txt
    except:
        print("Threre was an error in loading the file.")
    for line in rating.readlines():
        new_line=line.strip("\n")
        new_list.append(new_line)
        if i==1:
            ratings_dict[new_list[-2]]=dict_make(new_list[-1]) #call function to make a nested dictionary 
            i=0
        else:
            i+=1
    return ratings_dict #Format of the dictionary is {user:{1:5,2:0,3:0,4:3,5:0...}}     
        
        



#Start of program

final_dict={}
ratings_dict={}
books_list=[]
recommended_list=[]
found=False
final_list=[]
name_list=[]
k=0 #Variable incremented at the Most_suitable_user_list
books_list=book() #List of all the books and authors from "book.txt" file
ratings_dict=rating()#Make a dictionary of all the users and their ratings
num_books=number_of_recommenations() #Number of recommendations, default value is 10
user_name=input("Please enter the user name, ")
if user_name not in ratings_dict: #Check if user name exists in the ratings.txt file, else add a new user and his details 
    new_user_details(user_name,books_list)
    ratings_dict=rating() #Make a new ratings_dict with the added user
books_not_read=unread_books(user_name,ratings_dict) #Make a list of all the unread books of the user choosen
Most_suitable_user_list=similarity(user_name,books_list,ratings_dict) #Make a list of the most suitable users, descending order



print("We would like to recommend the following books for ",user_name)
while found == False: #Do while a result is not found
    recommended_list=[]
    suitable_books=books_rec(Most_suitable_user_list[k],books_list,ratings_dict)  #Find a list all books that are recommended by the compatible user
                            
    for i in books_not_read:
        if i in suitable_books:
            recommended_list.append(i) #make a list all books that are not read and are recommended by the compatible user   
    if len(recommended_list)==0: #If all suitable books are read by the user, skip the current user
        print("Here i came")
        k+=1 #Go to the next user
        continue
    else:
        k+=1
        final_dict,final_list,found=recommendations(final_dict,recommended_list,final_list,Most_suitable_user_list[k],num_books,found)    
    

if len(final_list) != num_books: #if number of recommendations are not met, then print the number of recommendations made.
    print("There were only ", len(final_list), "recommendations found")
    
with open("output.txt","a") as f: #Input to the output.txt file the result of the program
    f.write("Recommendations for "+ user_name + "\n" )
    print("Recommendations for "+ user_name + "\n" )
    for key,value in final_dict.items():
        if len(value)==0:
            continue
        else:
            print("From " + key + ". The following recommendations were made:" + "\n")
            f.write("From " + key + ". The following recommendations were made:"+ "\n")
            for i in value:
                f.write(str(books_list[i-1])+"\n")
                print(books_list[i-1])
    f.close()   
      
        
  
    
            
